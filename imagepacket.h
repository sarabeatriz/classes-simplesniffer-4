#ifndef IMAGEPACKET_H
#define IMAGEPACKET_H

#include <QByteArray>
#include <string>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QFileInfo>

using namespace std ;
///
/// \brief The imagepacket class is used to determine if an http packet
/// is an image request.
///
class imagepacket
{
    QString imageurl ; /**< store the image url / guarda el url de la imagen */
    QStringList extensions ; /**< list of image extensions / lista de extensiones de imagenes */

public:

    /// \fn imagepacket::imagepacket()
    /// \~English
    /// \brief Construct a list of known image extensions.
    /// \~Spanish
    /// \brief Construye una lista de extensiones de imagenes conocida.
    imagepacket();

    /// \fn bool imagepacket::isImage(string payload)
    /// \~English
    /// \brief Returns true if the http request payload received contains
    /// or is a request to an image, and stores the image url
    /// \param payload HTTP packet payload
    /// \return true if the http request payload received contains
    /// or is a request for an image
    /// \~Spanish
    /// \brief Devuelve cierto si la carga de la solicitud de HTTP recibida
    /// contiene o es una solicitud a una imagen, y guarda la imagen
    /// \param payload carga del paquete de HTTP
    /// \return cierto si la carga de la solicitud de HTTP recibida contiene
    /// o es una solicitud a una imagen
    bool isImage(string payload) ;

    /// \fn QString imagepacket::getImage()
    /// \~English
    /// \brief Returns an image url if found in an http request
    /// \return image url if found in an http request
    /// \brief Devuelve un url a una imagen si fue encontrada en una
    /// solicitud de http
    /// \return url de una imagen si encontrada en una solicitud de http
    QString getImage() ;
};

#endif // HTTPRESPONSEIMAGE_H
